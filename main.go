package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"

	_ "github.com/joho/godotenv/autoload"
)

//go:generate GOOS=windows GOARCH=amd64 go build

/*
	https://github.com/mattn/go-shellwords
	https://github.com/hashicorp/go-version
	https://github.com/go-git/go-git
*/

func main() {
	cmd := &cobra.Command{
		Use:     "releaser",
		Version: "0.4.3",
		Short:   "Gitlab Releaser",
		Long:    "A GitLab Release is a snapshot of the source, build output, artifacts, and other metadata associated with a released version of your code.",
		Run: func(cmd *cobra.Command, args []string) {
			var (
				serverURL        = os.Getenv("CI_SERVER_URL")
				apiURL           = os.Getenv("CI_API_V4_URL")
				apiToken         = os.Getenv("GITLAB_TOKEN")
				projectID        = os.Getenv("CI_PROJECT_ID")
				projectNamespace = os.Getenv("CI_PROJECT_NAMESPACE")
				projectName      = os.Getenv("CI_PROJECT_NAME")
				commitTag        = os.Getenv("CI_COMMIT_TAG")
				commitTitle      = os.Getenv("CI_COMMIT_TITLE")
				commitSHA        = os.Getenv("CI_COMMIT_SHA")
			)

			// gitLogArg
			gitLogArg := "--"

			// Get last tag
			if prev, err := gitExec("describe", "--tags", "--abbrev=0", fmt.Sprintf("%s^", commitTag)); err == nil {
				gitLogArg = fmt.Sprintf("%s..%s", strings.TrimSuffix(prev, "\n"), strings.TrimSuffix(commitTag, "\n"))
			}

			// Get git log
			gitLog, err := gitExec("log", "--pretty=oneline", "--abbrev-commit", "--no-decorate", "--no-color", gitLogArg)
			if err != nil {
				log.Fatal(err)
			}

			// Body values
			var bodyValues []string
			for _, line := range strings.Split(gitLog, "\n") {
				if len(line) > 0 {
					bodyValues = append(bodyValues, fmt.Sprintf("- %s", line))
				}
			}
			header := "## Changelog"
			body := strings.Join(bodyValues, "\n")
			message := strings.Join([]string{header, body}, "\n\n")

			// Gitlab client
			client, err := gitlab.NewClient(apiToken, gitlab.WithBaseURL(apiURL))
			if err != nil {
				log.Fatal(err)
			}

			// Release
			release, _, err := client.Releases.CreateRelease(projectID, &gitlab.CreateReleaseOptions{
				Name:        &commitTitle,
				Description: &message,
				Ref:         &commitSHA,
				TagName:     &commitTag,
			})
			if err != nil {
				log.Fatal(err)
			}
			log.Printf("Release %s for %s is created at %s", release.TagName, release.Commit.ShortID, release.CreatedAt.Format(time.RFC3339))

			for _, arg := range args {
				// Upload file
				projectFile, _, err := client.Projects.UploadFile(projectID, arg, nil)
				if err != nil {
					log.Print(err)
				}
				log.Printf("File %s uploaded", projectFile.Alt)

				// Link file
				fileLink := fmt.Sprintf("%s/%s/%s%s", serverURL, projectNamespace, projectName, projectFile.URL)
				releaseLinks, _, err := client.ReleaseLinks.CreateReleaseLink(projectID, commitTag, &gitlab.CreateReleaseLinkOptions{
					Name: &projectFile.Alt,
					URL:  &fileLink,
				})
				if err != nil {
					log.Fatal(err)
				}
				log.Printf("File %s linked", releaseLinks.Name)
			}
		},
	}

	_ = cmd.Execute()
}

func gitExec(args ...string) (result string, err error) {
	cmd := exec.Command("git", args...)

	stdout := bytes.Buffer{}
	stderr := bytes.Buffer{}

	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		return "", errors.New(stderr.String())
	}

	return stdout.String(), nil
}
