#!/bin/sh

GOOS=windows GOARCH=amd64 go build -o releaser-windows.exe
GOOS=linux GOARCH=amd64 go build -o releaser-linux
GOOS=darwin GOARCH=amd64 go build -o releaser-macos
