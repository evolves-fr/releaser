module gitlab.com/evolves-consulting/releaser

go 1.15

require (
	github.com/joho/godotenv v1.3.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/xanzy/go-gitlab v0.38.1
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
